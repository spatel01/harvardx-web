import datetime

from flask import Flask,render_template

app = Flask(__name__)

@app.route("/")
def index():
    n = datetime.datetime.now()

    kiteFestival =  n.day == 14 and n.month == 2
          
    return render_template("index.html", KiteFestival=kiteFestival)
