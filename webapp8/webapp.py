
from flask import Flask, render_template, request, session
from flask_session import Session

app = Flask(__name__)

app.config["SESSION_PERMANENT"] = False  # this is for 
app.config["SESSION_TYPE"] = "filesystem" # this is for 
Session(app)  # this for 

@app.route("/", methods=["GET", "POST"])
def index():
    if session.get("notes") is None:  # to check session exists or not
        session["notes"] = []
    if request.method == "POST":
        note = request.form.get("note")
        session["notes"].append(note)
        
    return render_template("index.html", notes=session["notes"])


