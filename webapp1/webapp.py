from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    return "<h1>Hello flask!!</h1>"

@app.route("/smit")
def smit():
    return "<h1>Hello Smit!!</h1>"
