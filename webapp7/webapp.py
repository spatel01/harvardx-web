
from flask import Flask,render_template, request

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("form.html")

@app.route("/hello", methods=["GET", "POST"])
def hello():
    if request.method == "GET":
        return "<h1>___Please submit a form instead___<h1>"
    else:

        username = request.form.get("username")
        email = request.form.get("email")
        password = request.form.get("password")
  
        l_data = [username, email, password]
        
        return render_template("hello.html", l_data=l_data)


