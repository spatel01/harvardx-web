from flask import Flask

app = Flask(__name__)

# whatever name we add after / it will be store value in name var and print it

@app.route("/")
def index():
    return "<h1>Hello flask!!</h1>"

@app.route("/<string:name>")  
def hello(name):
    return f"<h1>Hello {name}!!</h1>"


